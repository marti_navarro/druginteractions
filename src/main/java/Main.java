

import java.util.ArrayList;
import java.util.List;

import com.santabarbara.druginteractions.DrugInteractionConsultant;
import com.santabarbara.druginteractions.structures.ActivePrincipleInteraction;

import com.santabarbara.druginteractions.structures.DrugInteraction;

public class Main {

    public static void main(String[] args) throws Exception {
        
    	exampleGetActivePrincipleInteractionsFromDatabase();
    	
    	//exampleGetDrugInteractionsFromDatabase();
    	
    	//exampleGetActivePrincipleInteractionsFromWebService();
    	
    	//exampleGetDrugInteractionsFromWebService();
    }  
    
    private static void exampleGetActivePrincipleInteractionsFromDatabase()
    {
    	ArrayList <ActivePrincipleInteraction> list_active_principle_interaction = null;
    	
    	List <String> ListOfActivePrinciple = generateListATCCode();
  	
    	try {		
    		list_active_principle_interaction = DrugInteractionConsultant.getPrincipleActiveInteractionsFromDB(ListOfActivePrinciple);	
    		printActivePrincipleInteractionList(list_active_principle_interaction);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}	
    	
    } 
    
    private static void exampleGetDrugInteractionsFromDatabase()
    {
    	ArrayList <DrugInteraction> list_drug_interaction = null;
    	
    	List <String> ListOfDrug = generateListNationalCode();
    	
    	try {
    		list_drug_interaction = DrugInteractionConsultant.getDrugInteractionsFromDB(ListOfDrug);	
       		printDrugInteractionList(list_drug_interaction);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}	
    	
    } 
    
    private static void exampleGetActivePrincipleInteractionsFromWebService()
    {
    	ArrayList <ActivePrincipleInteraction> list_active_principle_interaction = null;
  
    	List <String> ListOfActivePrinciple = generateListATCCode();
    	
    	try {
    		list_active_principle_interaction = DrugInteractionConsultant.getPrincipleActiveInteractionsFromWS(ListOfActivePrinciple); 		
    		printActivePrincipleInteractionList(list_active_principle_interaction);
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    }
    
    private static void exampleGetDrugInteractionsFromWebService()
    {
    	ArrayList <DrugInteraction> list_drug_interaction = null;

    	List <String> ListOfDrug = generateListNationalCode();
    	
    	try {
    		list_drug_interaction = DrugInteractionConsultant.getDrugInteractionsFromWS(ListOfDrug);	
    		printDrugInteractionList(list_drug_interaction);		
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	}
    }
    
    private static List <String> generateListNationalCode()
    {
       	List <String> list_national_code = new ArrayList<String>();
    	list_national_code.add("7006336");
    	list_national_code.add("7000211");
    	list_national_code.add("6632604");
    	list_national_code.add("7494164");
    	list_national_code.add("0000000");
    	return list_national_code;
    }
    
    private static List <String> generateListATCCode()
    {
    	List <String> list_atc_code =new ArrayList<String>();
    	
    	list_atc_code.add("B05XA01");
    	list_atc_code.add("x");
    	list_atc_code.add("N02BA01");
    	return list_atc_code;
    }
    
    private static void printActivePrincipleInteractionList(ArrayList <ActivePrincipleInteraction> list_active_principle_interaction)
    {
		for(int i=0;i<list_active_principle_interaction.size();i++)
		{
			System.out.println(list_active_principle_interaction.get(i).toString());
		}
    }
    
    private static void printDrugInteractionList(ArrayList <DrugInteraction> list_drug_interaction)
    {
		for(int i=0;i<list_drug_interaction.size();i++)
		{
			System.out.println(list_drug_interaction.get(i).toString());
		}
    }
    
    
}
