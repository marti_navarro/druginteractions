package com.santabarbara.druginteractions;


import java.util.ArrayList;
import java.util.List;

import com.santabarbara.druginteractions.exceptions.DatabaseException;
import com.santabarbara.druginteractions.exceptions.NotResultException;
import com.santabarbara.druginteractions.exceptions.WebServiceException;
import com.santabarbara.druginteractions.sqldatabase.DBConnection;
import com.santabarbara.druginteractions.sqldatabase.DBDrugInteractionQuery;
import com.santabarbara.druginteractions.sqldatabase.DBActivePrincipleInteractionQuery;
import com.santabarbara.druginteractions.structures.ActivePrincipleInteraction;
import com.santabarbara.druginteractions.structures.DrugInteraction;
import com.santabarbara.druginteractions.webservices.WSActivePrincipleInteractionQuery;
import com.santabarbara.druginteractions.webservices.WSDrugInteractionQuery;

public class DrugInteractionConsultant {

	public static ArrayList <ActivePrincipleInteraction> getPrincipleActiveInteractionsFromDB(List <String> listATCCodes) throws DatabaseException
	{

		ArrayList <ActivePrincipleInteraction> list_active_principle_interaction = new ArrayList <ActivePrincipleInteraction>();	
		ActivePrincipleInteraction active_principle_interaction = null;

		DBConnection connection = new DBConnection();

		connection.openConnection();

		for(int i=0;i < listATCCodes.size()-1;i++)
		{
			for(int j=i+1; j <listATCCodes.size();j++)
			{	
				try {
					active_principle_interaction = DBActivePrincipleInteractionQuery.getInteractionFromDatabase(listATCCodes.get(i),listATCCodes.get(j), connection);
					list_active_principle_interaction.add(active_principle_interaction);	
				} catch (NotResultException ex) {
					
				} catch (DatabaseException ex) {
					ex.printStackTrace();
				}
			}
		}

		connection.closeConnection();

		return list_active_principle_interaction;
	}

	public static ArrayList <DrugInteraction> getDrugInteractionsFromDB(List <String> listNationalCodes) throws DatabaseException
	{		

		ArrayList <DrugInteraction> list_drug_interaction = new ArrayList <DrugInteraction>();
		DrugInteraction drug_interaction = null;

		DBConnection connection = new DBConnection();

		connection.openConnection();

		for(int i=0;i < listNationalCodes.size()-1;i++)
		{
			for(int j=i+1; j <listNationalCodes.size();j++)
			{	
				try
				{
					drug_interaction = DBDrugInteractionQuery.executeQuery(listNationalCodes.get(i),listNationalCodes.get(j), connection);
					list_drug_interaction.add(drug_interaction);	
				} catch (NotResultException ex) {

				} catch (Exception ex) {
					ex.printStackTrace();
					//throw new DatabaseException(ex.getMessage());
				}
			}
		}

		connection.closeConnection();

		return list_drug_interaction;
	}



	public static ArrayList <ActivePrincipleInteraction> getPrincipleActiveInteractionsFromWS(List <String> listATCCodes) throws WebServiceException
	{
		ArrayList <ActivePrincipleInteraction> list_active_principle_interaction = new ArrayList <ActivePrincipleInteraction>();
		ActivePrincipleInteraction active_principle_interaction = null;

		for(int i=0;i < listATCCodes.size()-1;i++)
		{
			for(int j=i+1; j <listATCCodes.size();j++)
			{	
				try {
					active_principle_interaction = WSActivePrincipleInteractionQuery.executeQuery(listATCCodes.get(i),listATCCodes.get(j));
					list_active_principle_interaction.add(active_principle_interaction);	
				} catch (NotResultException ex) {

				} catch (Exception ex) {
					throw new WebServiceException(ex.getMessage());
				}
			}
		}

		return list_active_principle_interaction;
	}

	public static ArrayList <DrugInteraction> getDrugInteractionsFromWS(List <String> listNationalCodes) throws WebServiceException
	{

		ArrayList <DrugInteraction> list_drug_interaction = new ArrayList <DrugInteraction>();
		DrugInteraction drug_interaction = null;

		for(int i=0;i < listNationalCodes.size()-1;i++)
		{
			for(int j=i+1; j <listNationalCodes.size();j++)
			{	
				try {
					drug_interaction = WSDrugInteractionQuery.executeQuery(listNationalCodes.get(i),listNationalCodes.get(j));
					list_drug_interaction.add(drug_interaction);	
				} catch (NotResultException ex) {

				} catch (Exception ex) {
					throw new WebServiceException(ex.getMessage());
				}
			}
		}

		return list_drug_interaction;
	}

}
