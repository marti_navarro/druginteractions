package com.santabarbara.druginteractions.sqldatabase;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.santabarbara.druginteractions.exceptions.DatabaseException;
import com.santabarbara.druginteractions.exceptions.NotResultException;

public class DBActivePrincipleInteractionQueryTest extends DBActivePrincipleInteractionQuery {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test 
	public void testDatabaseConnectionIsNull() {
		try {
			getInteractionFromDatabase("2", "", null);
			fail("ddd");
		} catch (NullPointerException e) {

		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("dd");
		} catch (NotResultException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("ddd");
		}
		//fail("Not yet implemented");
	}

	@Test 
	public void testFistCodeIsNull() {
		try {
			DBConnection connection = new DBConnection();
			connection.openConnection();		
			getInteractionFromDatabase(null, "", connection);
			fail("ddd");
		} catch (NullPointerException e) {
			
		}catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("ddd");
		} catch (NotResultException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("ddd");
		}
		//fail("Not yet implemented");
	}
	
	@Test 
	public void testSecondCodeIsNull() {
		try {
			DBConnection connection = new DBConnection();
			connection.openConnection();		
			getInteractionFromDatabase("", null, connection);
			fail("ddd");
		} catch (NullPointerException e) {
			
		}catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("ddd");
		} catch (NotResultException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("ddd");
		}
		//fail("Not yet implemented");
	}
}
