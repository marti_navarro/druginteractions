package com.santabarbara.druginteractions.sqldatabase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.santabarbara.druginteractions.exceptions.DatabaseException;
import com.santabarbara.druginteractions.exceptions.NotResultException;
import com.santabarbara.druginteractions.structures.ActivePrincipleInteraction;

public class DBActivePrincipleInteractionQuery {

	private static String QUERY = 
			"SELECT title, actions, nature, sense, significance, effect, " +
					"importance, mechanism, evidences, bibliography " + 
					"FROM active_principles_interaction " +
					"WHERE active_principles_interaction = " +
					"(SELECT active_principles_interaction_id FROM drugs_relation " +
					"WHERE code_act_pr1 = ? AND code_act_pr2 = ? LIMIT 1);";

	public static ActivePrincipleInteraction getInteractionFromDatabase(String atc_code_one, String atc_code_two, DBConnection connection) 
			throws NullPointerException, DatabaseException, NotResultException {

		if (atc_code_one == null) {
			throw new NullPointerException();
		}
		
		if (atc_code_two == null) {
			throw new NullPointerException();
		}
		
		if (connection == null) {
			throw new NullPointerException();
		}
		
		PreparedStatement ps;
		ResultSet query_result;
		ActivePrincipleInteraction active_principle_interaction = null;

		
		try {
			ps = getPreparedStatement(atc_code_one, atc_code_two, connection);

			query_result = ps.executeQuery();

			active_principle_interaction = processQueryResult(atc_code_one, atc_code_two, query_result);

			ps.close();
		} catch (SQLException ex)
		{
			throw new DatabaseException(ex.getMessage());
		}
		return active_principle_interaction;
	}

	private static PreparedStatement getPreparedStatement(String atc_code_one, String atc_code_two, DBConnection connection) 
			throws SQLException {

		PreparedStatement ps = connection.getConnection().prepareStatement(QUERY);
		ps.setString(1, atc_code_one);
		ps.setString(2, atc_code_two);

		return ps;
	}

	private static ActivePrincipleInteraction  processQueryResult(String atc_code_one, String atc_code_two, ResultSet rs) 
			throws NotResultException, SQLException {

		ActivePrincipleInteraction active_principle_interaction = null;

		active_principle_interaction = passResultSetToActivePrincipleInteraction(rs);
		active_principle_interaction.setATCCodeFirstActivePrinciple(atc_code_one);
		active_principle_interaction.setATCCodeSecondActivePrinciple(atc_code_two);

		return active_principle_interaction;
	}


	private static ActivePrincipleInteraction  passResultSetToActivePrincipleInteraction(ResultSet rs) 
			throws NotResultException, SQLException {

		ActivePrincipleInteraction active_principle_interaction = new ActivePrincipleInteraction();	

		if (rs.next()) {
			active_principle_interaction.setName(rs.getString(1));
			active_principle_interaction.setActionsToTake(rs.getString(2));
			active_principle_interaction.setNature(rs.getString(3));
			active_principle_interaction.setSenseInterference(rs.getString(4));
			active_principle_interaction.setClinicalSignificance(rs.getString(5));
			active_principle_interaction.setPharmacologicalEffect(rs.getString(6));
			active_principle_interaction.setImportance(rs.getString(7));
			active_principle_interaction.setMechanism(rs.getString(8));
			active_principle_interaction.setEvidences(rs.getString(9));
			active_principle_interaction.setReferences(rs.getString(10));
		} else {
			throw new NotResultException("not result in SQL query");
		}

		return active_principle_interaction;
	}
}
