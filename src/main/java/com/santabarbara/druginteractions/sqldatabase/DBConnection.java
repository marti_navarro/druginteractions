package com.santabarbara.druginteractions.sqldatabase;

import java.sql.Connection;
import java.sql.DriverManager;

import com.santabarbara.druginteractions.exceptions.DatabaseException;

public class DBConnection {

	private static final String DRIVER = "org.postgresql.Driver";	
	private static final String URI = "jdbc:postgresql://147.156.42.57:5432/DrugsInteraction";
	private static final String USER = "sergio";
	private static final String PASS = "tegratru";
	
	private Connection connection = null;

	public DBConnection() throws DatabaseException {
		
	}

	public void openConnection() throws DatabaseException
	{	
		try {
			Class.forName(DRIVER);
			if (connection == null) {
				connection = DriverManager.getConnection(URI,USER,PASS);
			}
		} catch (Exception ex) {	
			throw new DatabaseException(ex.getMessage());
		}
	}

	public  void closeConnection() throws DatabaseException
	{
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (Exception ex) {	
			throw new DatabaseException(ex.getMessage());
		}
	}
	
	public Connection getConnection() {
		return connection;
	}

}
