package com.santabarbara.druginteractions.sqldatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.santabarbara.druginteractions.exceptions.DatabaseException;
import com.santabarbara.druginteractions.exceptions.NotResultException;
import com.santabarbara.druginteractions.structures.DrugInteraction;

public class DBDrugInteractionQuery {

	public static DrugInteraction executeQuery(String national_code_one, String national_code_two, DBConnection connection) throws SQLException, NotResultException {

		String query;
		ResultSet resultados;
		DrugInteraction drug_interaction = null;

		query =  buildQuery(national_code_one,national_code_two);
		Statement s = connection.getConnection().createStatement();
		resultados = s.executeQuery(query);

		try {
			drug_interaction = processResult(resultados);
		} catch (NotResultException ex) {
			throw new NotResultException(ex.getMessage());
		} catch (DatabaseException ex) {
			ex.printStackTrace();
		}

		s.close();

		return drug_interaction;
	}
	
	
	private static String buildQuery(String national_code_one, String national_code_two) {
		
		String query = "SELECT medication1_code, medication1_name, code_act_pr1, name_act_pr1, " +
				        "medication2_code, medication2_name, code_act_pr2, name_act_pr2, clinical_significance " + 
	                    "FROM drugs_relation " +
	                    "WHERE (medication1_code = '" + national_code_one + 
	                    "' AND medication2_code = '" + national_code_two + "') " + 
	                    "OR (medication2_code = '" + national_code_one + 
	                    "' AND medication1_code = '" + national_code_two + "') " +
	                    " LIMIT 1;";
		
		return query;
	}
	
	
	private static DrugInteraction  processResult(ResultSet rs) throws DatabaseException, NotResultException {

		DrugInteraction drug_interaction = new DrugInteraction();	

		try {
			if (rs.next()) {
				drug_interaction.setCodeDrugOne(rs.getString(1));
				drug_interaction.setNameDrugOne(rs.getString(2));			
				drug_interaction.setCodeActivePrincipleOne(rs.getString(3));		
				drug_interaction.setNameActivePrincipleOne(rs.getString(4));
				drug_interaction.setCodeDrugTwo(rs.getString(5));
				drug_interaction.setNameDrugTwo(rs.getString(6));			
				drug_interaction.setCodeActivePrincipleTwo(rs.getString(7));		
				drug_interaction.setNameActivePrincipleTwo(rs.getString(8));
				drug_interaction.setClinicalSignificance(rs.getString(9));
			} else {
				throw new NotResultException("not result in SQL query");
			}
		} catch (SQLException ex) {
			throw new DatabaseException(ex.getMessage());
		}

		return drug_interaction;
	}
}
