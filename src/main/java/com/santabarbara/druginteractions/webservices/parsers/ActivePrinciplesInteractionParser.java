/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.santabarbara.druginteractions.webservices.parsers;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.santabarbara.druginteractions.structures.ActivePrincipleInteraction;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class ActivePrinciplesInteractionParser extends DefaultHandler {

	private ActivePrincipleInteraction ipa = null;
	String aux = null;

	/**
	 * boolean tags
	 */
	boolean bTITULO = false;
	boolean bMEDIDAS = false;
	boolean bNATURALEZA = false;
	boolean bSENTIDO = false;
	boolean bSIGN = false;
	boolean bEFECTO = false;
	boolean bIMP = false;
	boolean bMECANISMO = false;
	boolean bEVIDENCIAS = false;
	boolean bREFERENCIAS = false;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (qName.equalsIgnoreCase("registro")) {
			ipa = new ActivePrincipleInteraction();
		} else if (qName.equalsIgnoreCase("titulo")) {
			bTITULO = true;
		} else if (qName.equalsIgnoreCase("medidas")) {
			bMEDIDAS = true;
		} else if (qName.equalsIgnoreCase("naturaleza")) {
			bNATURALEZA = true;
		} else if (qName.equalsIgnoreCase("sentido")) {
			bSENTIDO = true;
		} else if (qName.equalsIgnoreCase("significacion")) {
			bSIGN = true;
		} else if (qName.equalsIgnoreCase("efecto")) {
			bEFECTO = true;
		} else if (qName.equalsIgnoreCase("importancia")) {
			bIMP = true;
		} else if (qName.equalsIgnoreCase("mecanismo")) {
			bMECANISMO = true;
		} else if (qName.equalsIgnoreCase("evidencias")) {
			bEVIDENCIAS = true;
		} else if (qName.equalsIgnoreCase("referencias")) {
			bREFERENCIAS = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (qName.equalsIgnoreCase("efecto")) {
			bEFECTO = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setPharmacologicalEffect(html.body().text());
					aux = null;
					return;
				}
				ipa.setPharmacologicalEffect(aux);
			}
			else
				ipa.setPharmacologicalEffect(null);
			aux = null;
		} else if (qName.equalsIgnoreCase("importancia")) {
			bIMP = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setImportance(html.body().text());
					aux = null;
					return;
				}
				ipa.setImportance(aux);
			}
			else
				ipa.setImportance(null);
			aux = null;
		} else if (qName.equalsIgnoreCase("titulo")) {
			bTITULO = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setName(html.body().text());
					aux = null;
					return;
				}
				ipa.setName(aux);
			}
			else
				ipa.setName(null);
			aux = null;
		} else if (qName.equalsIgnoreCase("medidas")) {
			bMEDIDAS = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setActionsToTake(html.body().text());
					aux = null;
					return;
				}
				ipa.setActionsToTake(aux);
			}
			else
				ipa.setActionsToTake(null);
			aux = null;
		} else if (qName.equalsIgnoreCase("naturaleza")) {
			bNATURALEZA = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setNature(html.body().text());
					aux = null;
					return;
				}
				ipa.setNature(aux);
			}
			else
				ipa.setNature(aux);
			aux = null;
		} else if (qName.equalsIgnoreCase("sentido")) {
			bSENTIDO = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setSenseInterference(html.body().text());
					aux = null;
					return;
				}
				ipa.setSenseInterference(aux);
			}
			else
				ipa.setSenseInterference(null);
			aux = null;
		} else if (qName.equalsIgnoreCase("significacion")) {
			bSIGN = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setClinicalSignificance(html.body().text());
					aux = null;
					return;
				}
				ipa.setClinicalSignificance(aux);
			}
			else
				ipa.setClinicalSignificance(null);
			aux = null;
		} else if (qName.equalsIgnoreCase("mecanismo")) {
			bMECANISMO = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setMechanism(html.body().text());
					aux = null;
					return;
				}
				ipa.setMechanism(aux);
			}
			else
				ipa.setMechanism(null);
			aux = null;
		} else if (qName.equalsIgnoreCase("evidencias")) {
			bEVIDENCIAS = false;
			if(aux != null){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setEvidences(html.body().text());
					aux = null;
					return;
				}
				ipa.setEvidences(aux);
			}
			else
				ipa.setEvidences(null);
			aux = null;
		} else if (qName.equalsIgnoreCase("referencias")) {
			bREFERENCIAS = false;
			if(aux != null ){
				if (aux.charAt(0) == '<'){
					Document html = Jsoup.parse(aux);
					ipa.setReferences(html.body().text());
					aux = null;
					return;
				}
				ipa.setReferences(aux);
			}
			else
				ipa.setReferences(null);
			aux = null;
		}
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {

		if (aux != null) {
			aux = aux + new String(ch, start, length);
		} else {
			aux = new String(ch, start, length);
		}
	}

	public ActivePrincipleInteraction getInteraccion() {
		return ipa;
	}
}
