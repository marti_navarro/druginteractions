/* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.santabarbara.druginteractions.webservices.parsers;

import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.santabarbara.druginteractions.structures.DrugInteraction;

public class DrugInteractionParser extends DefaultHandler {

	private StringBuffer buffer;
	private ArrayList<DrugInteraction> relaciones;
	private int index;

	public DrugInteractionParser()
	{
		super();
		buffer = new StringBuffer();
		relaciones = new ArrayList<DrugInteraction>();
		index = 0;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		buffer.setLength(0);
		if (qName.equalsIgnoreCase("registro")) {
			DrugInteraction r = new DrugInteraction();
			relaciones.add(r);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (qName.equalsIgnoreCase("registro")) {
			index++;
		} else if (qName.equalsIgnoreCase("significacionclinica")) {
			relaciones.get(index).setClinicalSignificance(buffer.toString().trim());
		} else if (qName.equalsIgnoreCase("codigomedicamento1")) {
			relaciones.get(index).setCodeDrugOne(buffer.toString().trim());
		} else if (qName.equalsIgnoreCase("medicamento1")) {
			relaciones.get(index).setNameDrugOne(buffer.toString().trim());
		} else if (qName.equalsIgnoreCase("codigoprincipioactivo1")) {
			relaciones.get(index).setCodeActivePrincipleOne(buffer.toString().trim());
		} else if (qName.equalsIgnoreCase("principioactivo1")) {
			relaciones.get(index).setNameActivePrincipleOne(buffer.toString().trim());
		} else if (qName.equalsIgnoreCase("codigomedicamento2")) {
			relaciones.get(index).setCodeDrugTwo(buffer.toString().trim());
		} else if (qName.equalsIgnoreCase("medicamento2")) {
			relaciones.get(index).setNameDrugTwo(buffer.toString().trim());
		} else if (qName.equalsIgnoreCase("codigoprincipioactivo2")) {
			relaciones.get(index).setCodeActivePrincipleTwo(buffer.toString().trim());
		} else if (qName.equalsIgnoreCase("principioactivo2")) {
			relaciones.get(index).setNameActivePrincipleTwo(buffer.toString().trim());
		}
	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {

		// StackOverflow: http://stackoverflow.com/questions/13602752/xml-empty-tags-using-sax-parsing
		buffer.append(ch, start, length);
	}

	public ArrayList<DrugInteraction> getrelaciones() {
		return relaciones;
	}
}
