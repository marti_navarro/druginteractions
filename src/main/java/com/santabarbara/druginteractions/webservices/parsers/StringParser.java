/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.santabarbara.druginteractions.webservices.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class StringParser extends DefaultHandler {

	boolean bString = false;
	String xmlBienFormado;

	@Override
	public void startElement(String uri,String localName, String qName, Attributes attributes) throws SAXException {

		if (qName.equalsIgnoreCase("string")) {
			bString = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

	}

	@Override
	public void characters(char ch[], int start, int length) throws SAXException {

		if (bString) {
			if (xmlBienFormado != null) {
				xmlBienFormado = xmlBienFormado + new String(ch, start, length);
			} else {
				xmlBienFormado = new String(ch, start, length);
			}
		}
	}

	public String getXMLBienFormado() {
		return xmlBienFormado;
	}
}
