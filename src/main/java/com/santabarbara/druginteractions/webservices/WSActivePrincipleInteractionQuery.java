package com.santabarbara.druginteractions.webservices;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import org.xml.sax.InputSource;

import com.santabarbara.druginteractions.exceptions.NotResultException;
import com.santabarbara.druginteractions.exceptions.WebServiceException;
import com.santabarbara.druginteractions.structures.ActivePrincipleInteraction;
import com.santabarbara.druginteractions.webservices.parsers.ActivePrinciplesInteractionParser;
import com.santabarbara.druginteractions.webservices.parsers.StringParser;

public class WSActivePrincipleInteractionQuery {
	
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String WEB_PORTAL = "http://swbdm.portalfarma.com/WebServiceBDM.asmx/BotPlus2";
	private static final String ID_VERIFICATION_USER = "cHJ1ZWJhaG9zcGl0YWxzYWx1ZA";
	private static final String USER = "4usFs3lLXbT1i";
	private static final String ID_SERVICE = "2Woka/m+KaCC6ptWw+BM7d4";
	
	public static ActivePrincipleInteraction executeQuery(String atc_code_one, String atc_code_two) throws NotResultException {
		
		ActivePrincipleInteraction active_principle_interaction = null;
		String wellFormedXML = null;
	
		try {
			wellFormedXML = queryXMLwithActivePrincipleInteractionFromWebService(atc_code_one, atc_code_two);
			active_principle_interaction = putXMLinActivePrincipleInteraction(wellFormedXML);				
		} catch (WebServiceException ex) {		
			throw new NotResultException(ex.getMessage());
		} 
			
		return active_principle_interaction;
	}
	
    public static  String queryXMLwithActivePrincipleInteractionFromWebService(String codeOne, String codeTwo) throws  WebServiceException{

    	HttpPost post = null;
    	String badformedXML = null;
    	String wellformedXML = null;

    	try {
    		post = buildTheQuery(codeOne, codeTwo);
    		badformedXML = executeTheQuery(post);
    		wellformedXML = formatXMLfile(badformedXML); 			
    	} catch (Exception ex) {
    		throw new WebServiceException(ex.getMessage());
    	}

    	return wellformedXML;       
    }
    
	private static HttpPost buildTheQuery(String codeOne, String codeTwo) throws UnsupportedEncodingException {
		
		HttpPost post;
		List<NameValuePair> urlParameters;
		 
		post = new HttpPost(WEB_PORTAL);
        post.setHeader("User-Agent", USER_AGENT);   
        
        urlParameters = new ArrayList<NameValuePair>();
        urlParameters.add(new BasicNameValuePair("idverifuser", ID_VERIFICATION_USER));
        urlParameters.add(new BasicNameValuePair("user", USER));
        urlParameters.add(new BasicNameValuePair("idservice", ID_SERVICE));
        urlParameters.add(new BasicNameValuePair("parametros", codeOne));
        urlParameters.add(new BasicNameValuePair("parametros", codeTwo));
        
        post.setEntity(new UrlEncodedFormEntity(urlParameters));  
        
		return post;
	}
	
	private static String executeTheQuery(HttpPost post) throws IOException
	{
		CloseableHttpClient client;
		HttpResponse response;
		String parsedresponse;

		client = HttpClientBuilder.create().build();    
		response = client.execute(post);
		parsedresponse = passResponseToString(response);

		return parsedresponse;
	}
	
	private static String passResponseToString(HttpResponse response)  throws IOException
	{

		BufferedReader rd;
		StringBuffer result;
		String line = "";

		rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));

		result = new StringBuffer();
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		return (result.toString());
	}
	   
    private static String formatXMLfile (String XML) throws WebServiceException {
        
        String xmlBienFormado = null;       
      
        try {       	
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            StringParser formString = new StringParser();
            saxParser.parse(new InputSource(new ByteArrayInputStream(XML.getBytes("utf-8"))), formString);
            xmlBienFormado = formString.getXMLBienFormado();
            if(xmlBienFormado.charAt(0) != '<') {
                return null;  //TODO: no es buena practica devolver null, cambiar
            }
            
            xmlBienFormado = "<?xml version=\"1.1\"?>" + xmlBienFormado;
                       
        } catch (Exception ex) {
        	throw new WebServiceException(ex.getMessage());
        }
        
        return xmlBienFormado;
    }  
    
    private static ActivePrincipleInteraction putXMLinActivePrincipleInteraction(String XMLFile) throws WebServiceException
    {
    	ActivePrincipleInteraction active_principle_interaction = null;

    	try {
    		SAXParserFactory factory = SAXParserFactory.newInstance();
    		SAXParser saxParser = factory.newSAXParser(); 
    		ActivePrinciplesInteractionParser userhandler = new ActivePrinciplesInteractionParser();
    		saxParser.parse(new InputSource(new ByteArrayInputStream(XMLFile.getBytes("utf-8"))), userhandler);
    		active_principle_interaction = userhandler.getInteraccion();  
    	} catch (Exception ex) {
    		throw new WebServiceException(ex.getMessage());
    	}
    	
    	return active_principle_interaction;
    }
}
	
