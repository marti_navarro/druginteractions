package com.santabarbara.druginteractions.exceptions;

public class WebServiceException extends Exception {
	
	private static final long serialVersionUID = 1326222097432049204L;

	public WebServiceException(String msg) {
		super(msg);
	}
}
