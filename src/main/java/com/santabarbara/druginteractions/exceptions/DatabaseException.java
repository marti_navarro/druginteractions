package com.santabarbara.druginteractions.exceptions;

public class DatabaseException extends Exception {

	private static final long serialVersionUID = -3046489910681922204L;

	public DatabaseException(String msg) {
		super(msg);
	}
}
