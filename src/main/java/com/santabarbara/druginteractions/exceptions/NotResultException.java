package com.santabarbara.druginteractions.exceptions;

public class NotResultException extends Exception {


	private static final long serialVersionUID = 7776076837225912274L;

	public NotResultException(String msg) {
		super(msg);
	}
}
