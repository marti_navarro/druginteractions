package com.santabarbara.druginteractions.structures;

public class DrugInteraction {


    private String clinical_significance;
    private String code_drug_one;
    private String name_drug_one;
    private String code_active_principle_one;
    private String name_active_principle_one; 
    private String code_drug_two;
    private String name_drug_two;
    private String code_active_principle_two;
    private String name_active_principle_two;
 
    public DrugInteraction() {
    
    }

    public String getClinicalSignificance() {
        return clinical_significance;
    }

    public void setClinicalSignificance(String clinical_sig) {
        this.clinical_significance = clinical_sig;
    }

    public String getCodeDrugOne() {
        return code_drug_one;
    }

    public void setCodeDrugOne(String code_drug_one) {
        this.code_drug_one = code_drug_one;
    } 
    
    public String getCodeDrugTwo() {
        return code_drug_two;
    }

    public void setCodeDrugTwo(String code_drug_two) {
        this.code_drug_two = code_drug_two;
    }     
    
    public String getNameDrugOne() {
        return name_drug_one;
    }

    public void setNameDrugOne(String name_drug_one) {
        this.name_drug_one = name_drug_one;
    }
    
    public String getNameDrugTwo() {
        return name_drug_two;
    }

    public void setNameDrugTwo(String name_drug_two) {
        this.name_drug_two = name_drug_two;
    }    
    
    public String getCodeActivePrincipleOne() {
        return code_active_principle_one;
    }

    public void setCodeActivePrincipleOne(String code_active_principle_one) {
        this.code_active_principle_one = code_active_principle_one;
    }   
    
    public String getCodeActivePrincipleTwo() {
        return code_active_principle_two;
    }

    public void setCodeActivePrincipleTwo(String code_active_principle_two) {
        this.code_active_principle_two = code_active_principle_two;
    }
          
    public String getNameActivePrincipleOne() {
        return name_active_principle_one;
    }

    public void setNameActivePrincipleOne(String name_active_principle_one) {
        this.name_active_principle_one = name_active_principle_one;
    }
    
    public String getNameActivePrincipleTwo() {
        return name_active_principle_two;
    }

    public void setNameActivePrincipleTwo(String name_active_principle_two) {
        this.name_active_principle_two = name_active_principle_two;
    }
    

    @Override  
    public String toString() {
    	
    	String interactionString;
    	  	
        interactionString = "Drug Interaction : {" + 
        "Code Drug One: " + code_drug_one +
        ", Name Drug One: " + name_drug_one +
        ", Code Active Principle Drug One: " + code_active_principle_one + 
        ", Name Active Principle Drug One: " + name_active_principle_one + 
        ", Code Drug Two: " + code_drug_two + 
        ", Name Drug Two: " + name_drug_two + 
        ", Code Active Principle Drug Two: " + code_active_principle_two +
        ", Name Active Principle Drug Two: " + name_active_principle_two + 
        ", Clinical Signivicance: " + clinical_significance +
        "}";
        
        return interactionString;
    }
}
