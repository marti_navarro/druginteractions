package com.santabarbara.druginteractions.structures;

public class ActivePrincipleInteraction {

	
	String atc_code_first_active_principle;
	String atc_code_second_active_principle;  
    String name;
    String actions_to_take;
    String nature;
    String sense_interference;
    String clinical_significance;
    String pharmacological_effect;
    String importance;
    String mechanism;
    String evidences; 
    String references;
    
    public ActivePrincipleInteraction() {
    	
    }

	public String getATCCodeFirstActivePrinciple() {
		return atc_code_first_active_principle;
	}

	public void setATCCodeFirstActivePrinciple(String atc_code_first_active_principle) {
		this.atc_code_first_active_principle = atc_code_first_active_principle;
	}

	public String getATCCodeSecondActivePrinciple() {
		return atc_code_second_active_principle;
	}

	public void setATCCodeSecondActivePrinciple(String atc_code_second_active_principle) {
		this.atc_code_second_active_principle = atc_code_second_active_principle;
	}
	
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActionsToTake() {
        return actions_to_take;
    }

    public void setActionsToTake(String actions) {
        this.actions_to_take = actions;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getSenseInterference() {
        return sense_interference;
    }

    public void setSenseInterference(String sense) {
        this.sense_interference = sense;
    }

    public String getClinicalSignificance() {
        return clinical_significance;
    }

    public void setClinicalSignificance(String significance) {
        this.clinical_significance = significance;
    }

    public String getPharmacologicalEffect() {
        return pharmacological_effect;
    }

    public void setPharmacologicalEffect(String effect) {
        this.pharmacological_effect = effect;
    }

    public String getImportance() {
        return importance;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }

    public String getMechanism() {
        return mechanism;
    }

    public void setMechanism(String mechanism) {
        this.mechanism = mechanism;
    }

    public String getEvidences() {
        return evidences;
    }

    public void setEvidences(String evidences) {
        this.evidences = evidences;
    }

    public String getReferences() {
        return references;
    }

    public void setReferences(String references) {
        this.references = references;
    }

    @Override
    public String toString() {
    	
    	String interactionString;
    	  	
        interactionString = "Active Principle Interaction : {" + 
        "ATC Code one: " + atc_code_first_active_principle +
        ", ATC Code two: " + atc_code_second_active_principle +
        ", Name: " + name + 
        ", Actions: " + actions_to_take + 
        ", Nature: " + nature + 
        ", Sense of Interference: " + sense_interference + 
        ", Clinical Significance: " + clinical_significance +
        ", Pharmacological Effect: " + pharmacological_effect + 
        ", Importance: " + importance +
        ", Mechanism: " + mechanism + 
        ", Evicences: " + evidences + 
        ", References: " + references + 
        "}";
        
        return interactionString;
    }
}
